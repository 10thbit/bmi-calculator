import 'package:flutter/material.dart';

class CardDesign extends StatelessWidget {

  double left, top, right, bottom;
  Widget cardChild;
  Color cardColour;
  final Function onPress;

  CardDesign({required this.cardColour, required this.left, required this.top, required this.right, required this.bottom, required this.cardChild, required this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onPress(),
      child: Container(
        margin: EdgeInsets.fromLTRB(left, top, right, bottom),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: cardColour,
        ),
        child: cardChild,
      ),
    );
  }
}