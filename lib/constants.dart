import 'package:flutter/material.dart';

const kActiveCardColor = Color(0xFF1D1E33);
const kInactiveCardColor = Color(0xFF111328);

const kGenderTextStyle = TextStyle(
  fontSize: 20.0,
  color: Colors.grey
);

const kNumbersTextStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.w900,
);