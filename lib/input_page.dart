import 'package:bmi_calculator/result_calculator.dart';
import 'package:bmi_calculator/main.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'card_design.dart';
import 'gender_card.dart';
import 'constants.dart';
import 'results.dart';
import 'scale_button.dart';

enum Gender{
  male,
  female,
}

class InputPage extends StatefulWidget {
  const InputPage({Key? key}) : super(key: key);

  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {

  Gender? selectedGender;
  int height = 120;
  int weight = 60;
  int age = 22;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        title: Text("BMI Calculator",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 1,
            child: Row(
              children: [
                Expanded(
                  child: CardDesign(
                    left: 15,
                    top: 15,
                    right: 5,
                    bottom: 5,
                    cardColour: selectedGender == Gender.male ? kActiveCardColor : kInactiveCardColor,
                    cardChild: GenderCard(
                      genderIcon: FontAwesomeIcons.mars,
                      genderText: "MALE",
                    ),
                    onPress: (){
                      setState(() {
                        selectedGender = Gender.male;
                      });
                    },
                  ),
                ),
                Expanded(
                  child: CardDesign(
                    left: 5,
                    top: 15,
                    right: 15,
                    bottom: 5,
                    cardColour: selectedGender == Gender.female ? kActiveCardColor : kInactiveCardColor,
                    cardChild: GenderCard(
                      genderIcon: FontAwesomeIcons.venus,
                      genderText: "FEMALE",
                    ),
                    onPress: (){
                      setState(() {
                        selectedGender = Gender.female;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
                child: CardDesign(left: 15,top: 5,right: 15,bottom: 5, cardColour: kInactiveCardColor,
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("HEIGHT",
                      style: kGenderTextStyle,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        textBaseline: TextBaseline.alphabetic,
                        children: [
                          Text(
                            height.toString(),
                            style: kNumbersTextStyle
                          ),
                          Text(
                            "cm",
                            style: kGenderTextStyle,
                          )
                        ],
                      ),
                      SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          thumbColor: Colors.pink,
                          overlayColor: Color(0x26EB1555),
                          activeTrackColor: Colors.white,
                          inactiveTrackColor: Colors.grey,
                          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15),
                          overlayShape: RoundSliderOverlayShape(overlayRadius: 26),
                        ),
                        child: Slider(
                          value: height.toDouble(),
                          min: 100.0,
                          max: 300.0,
                          onChanged: (double newValue) {
                            setState(() {
                              height= newValue.round();
                            });
                          },
                        ),
                      )
                    ],
                  ),
                  onPress: (){

                  },
                ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              children: [
                Expanded(
                  child: CardDesign(left: 15,top: 5,right: 5,bottom: 15, cardColour: kInactiveCardColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "WEIGHT",
                          style: kGenderTextStyle,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.alphabetic,
                          children: [
                            Text(
                              weight.toString(),
                              style: kNumbersTextStyle,
                            ),
                            Text(
                              "kg",
                              style: kGenderTextStyle,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: [
                            ScaleButton(
                              onPress: (){
                                setState(() {
                                  weight<1 ? "" : weight--;
                                });
                              },
                              iconState: Icons.remove,
                            ),
                            ScaleButton(
                                onPress: (){
                                  setState(() {
                                    weight++;
                                  });
                                },
                                iconState: Icons.add,
                            ),
                          ],
                        )
                      ],
                    ),
                    onPress: (){

                    },
                  ),
                ),
                Expanded(
                  child: CardDesign(left: 5,top: 5,right: 15,bottom: 15, cardColour: kInactiveCardColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "AGE",
                          style: kGenderTextStyle,
                        ),
                        Text(
                          age.toString(),
                          style: kNumbersTextStyle,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: [
                            ScaleButton(
                              onPress: (){
                                setState(() {
                                  age<1 ? "" : age--;
                                });
                              },
                              iconState: Icons.remove,
                            ),
                            ScaleButton(
                              onPress: (){
                                setState(() {
                                  age++;
                                });
                              },
                              iconState: Icons.add,
                            ),
                          ],
                        )
                      ],
                    ),
                    onPress: (){

                    },
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: (){
              ResultCalculator resultCalculator = ResultCalculator(height, weight);


                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){
                    return Results(
                      bmiResult: resultCalculator.calculateBMI(),
                      bmiResultStatus: resultCalculator.getResult(),
                      bmiResultInterpretation: resultCalculator.getInterpretation(),
                    );
                  },
                  ),
                );
            },
            child: Container(
              height: 60.0,
              width: double.infinity,
              color: Colors.pink,
              margin: EdgeInsets.only(top: 5),
              child: Center(
                child: Text(
                  "CALCULATE",
                  style: kGenderTextStyle.copyWith(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}