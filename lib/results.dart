import 'package:bmi_calculator/constants.dart';
import 'package:flutter/material.dart';
import 'card_design.dart';
import 'result_calculator.dart';

class Results extends StatelessWidget {

  final String bmiResult;
  final String bmiResultStatus;
  final String bmiResultInterpretation;


  Results({required this.bmiResult, required this.bmiResultStatus, required this.bmiResultInterpretation});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
              "BMI Results"
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: Container(
          margin: EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                "Your Results",
                style: kGenderTextStyle.copyWith(
                  color: Colors.white,
                  fontSize: 45.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Expanded(
                child: CardDesign(
                  left: 5,
                  top: 5,
                  right: 5,
                  bottom: 5,
                  cardColour: kInactiveCardColor,
                  cardChild: Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          bmiResultStatus,
                          style: kGenderTextStyle.copyWith(
                            color: Colors.green.shade500,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(bmiResult,
                          style: kNumbersTextStyle.copyWith(
                              fontSize: 80.0
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Text(
                            bmiResultInterpretation,
                            style: kGenderTextStyle.copyWith(
                              letterSpacing: 1.0,
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    ),
                  ),
                  onPress: (){

                  },
                ),
              ),
              GestureDetector(
                onTap: (){
                    Navigator.pop(context);
                },
                child: Container(
                  height: 60.0,
                  width: double.infinity,
                  color: Colors.pink,
                  margin: EdgeInsets.only(top: 15),
                  child: Center(
                    child: Text(
                      "RE-CALCULATE",
                      style: kGenderTextStyle.copyWith(
                        color: Colors.white,
                        fontSize: 30,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        )
    );

}
}
