import 'package:flutter/material.dart';

class ScaleButton extends StatelessWidget {

  final Function onPress;
  final IconData iconState;

  ScaleButton({required this.onPress, required this.iconState});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: CircleBorder(),
      color: Colors.grey.shade500,
      child: Icon(
        iconState,
        size: 55.0,
      ),
      onPressed: () => onPress(),
    );
  }
}